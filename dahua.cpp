#include <iostream>
#include <sstream>
#include <dlfcn.h>
#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h> 

#include "dhnetsdk.h"
#include "functions.h"

using namespace std;

string dirname;


void CALLBACK SnapRev(LLONG lLoginID, BYTE *pBuf, UINT RevLen, UINT EncodeType,
DWORD CmdSerial, LDWORD dwUser){
	cout << "[" << CmdSerial << "] " << "Got " << RevLen << " bytes of ";
	if (EncodeType == 10){
		cout << "JPEG\n";
	}
	else if (EncodeType == 0){
		cout << "I frame of MPEG-4\n";
	}
	FILE *pFile;
	stringstream ss;
	ss << "./" << dirname.c_str() << "/ch_" << CmdSerial + 1 << "_.jpeg";
	string file_name = ss.str();
	pFile = fopen(file_name.c_str(), "wb");
	fwrite(pBuf, 1, RevLen, pFile);
	fclose(pFile);
}


int main(int argc, char* argv[]) {

    using std::cout;
    using std::cerr;

    if(argc < 4){
    	cout << "Usage:\nscreengrab <ip> <port> <user> <password>\n";
    	return 1;
    }

    // string i_ip;
    // string i_user;
    // string i_password;

    // cout << "IP: ";
    // cin >> i_ip;
    // dirname = i_ip;
    // cout << "User: ";
    // cin >> i_user;
    // cout << "Password: ";
    // cin >> i_password;

    // open the library
    cout << "Opening libdhnetsdk.so...\n";
    void* handle = dlopen("./lib/libdhnetsdk.so", RTLD_LAZY);
    
    if (!handle) {
        cerr << "Cannot open library: " << dlerror() << '\n';
        return 1;
    }
    
    //load symbols
    dlerror();
    CLIENT_Init_t client_i = (CLIENT_Init_t) dlsym(handle, "CLIENT_Init");
    CLIENT_GetSDKVersion_t get_sdk_version = (CLIENT_GetSDKVersion_t) dlsym(handle, "CLIENT_GetSDKVersion");
    CLIENT_LoginEx2_t login = (CLIENT_LoginEx2_t) dlsym(handle, "CLIENT_LoginEx2");
    CLIENT_Cleanup_t cleanup = (CLIENT_Cleanup_t) dlsym(handle, "CLIENT_Cleanup");
    CLIENT_SetSnapRevCallBack_t set_snap_callback = (CLIENT_SetSnapRevCallBack_t) dlsym(handle, "CLIENT_SetSnapRevCallBack");
    CLIENT_SnapPictureEx_t snap_picture = (CLIENT_SnapPictureEx_t) dlsym(handle, "CLIENT_SnapPictureEx");
    CLIENT_GetLastError_t get_last_error = (CLIENT_GetLastError_t) dlsym(handle, "CLIENT_GetLastError");

    const char *dlsym_error = dlerror();
    if (dlsym_error) {
        cerr << dlsym_error << '\n';
        dlclose(handle);
        return 1;
    }
    
    // and here we go

    dirname = argv[1];

    DIR           *dirp;
    struct dirent *directory;

    dirp = opendir(".");
    if (dirp)
    {
        while ((directory = readdir(dirp)) != NULL)
        {

          if (directory->d_name == dirname){
          	cout << "Removing old directory\n";
          	stringstream ss;
          	ss << "rm -rf " << directory->d_name;
          	system(ss.str().c_str());
          }
        }

        closedir(dirp);
    }


    client_i();
    cout << "SDK version is: " << get_sdk_version() << "\n";
    int err = 0;

    // cout << argv[0] << " " << argv[1] << " " << argv[2] << " " << argv[3] << " " << argv[4] << "";

    const char *ip = argv[1];
    WORD port = atoi(argv[2]);
    const char *user = argv[3];
    const char *password = argv[4];

    NET_DEVICEINFO_Ex stDevInfoEx = {0};

    LLONG login_id = login(ip, port, user, password, EM_LOGIN_SPEC_CAP_TCP, NULL, &stDevInfoEx, &err);
    if (login_id == 0){
    	cout << "FAIL\n";
    	return 1;
    }
    cout << "LoginID: " << login_id << "\n";
   	cout << "Detected " << stDevInfoEx.nChanNum << " channels\n";
   	const int dir_err = mkdir(argv[1], S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	if (-1 == dir_err)
	{
    	printf("Error creating directory!\n");
    	return 1;
	}
    for (int i = 0; i < stDevInfoEx.nChanNum; i++){
    	set_snap_callback(SnapRev, 0);
    	SNAP_PARAMS stuSnapParams;
    	stuSnapParams.Channel = i;
		stuSnapParams.mode = 0;
		stuSnapParams.CmdSerial = i;
		stuSnapParams.Quality = 6;
		int reserved = 0;
		if (false == snap_picture(login_id, &stuSnapParams, &reserved)){
			cout << "CLIENT_SnapPictureEx error " << get_last_error() << "\n";
			return 1;
		}
		usleep(600000);
	}
	usleep(700000);
	if (stDevInfoEx.nChanNum > 8){
		sleep(2);
	}
    cleanup();
    dlclose(handle);
}

