typedef bool (*CLIENT_Init_t)();
typedef DWORD (*CLIENT_GetSDKVersion_t)();
typedef LLONG (*CLIENT_LoginEx2_t)(
		const char *pchDVRIP,
		WORD wDVRPort,
		const char *pchUserName,
		const char *pchPassword,
		EM_LOGIN_SPAC_CAP_TYPE emSpecCap,
		void* pCapParam,
		LPNET_DEVICEINFO_Ex lpDeviceInfo,
		int *error
		);
typedef void (*CLIENT_Cleanup_t)();

typedef void (*CLIENT_SetSnapRevCallBack_t)(
	fSnapRev OnSnapRevMessage,
	LDWORD dwUser
	);

typedef bool (*CLIENT_SnapPictureEx_t)(
	LLONG lLoginID,
	SNAP_PARAMS *par,
	int *reserved
);

typedef DWORD (*CLIENT_GetLastError_t)(void);